#from pylab import *
from random import *
from statistics import *

# !/usr/bin/python

def readtable(name):
    f = open(name, 'r')
    lines = f.readlines()
    result = []
    for x in lines:
        result.append(x)
    f.close()
    tabela = []
    for x in range(0,len(result)):
        mydata=filter(None, (result[x].strip()).split(" ") )
#       mydata = filter(None, mydata)
        if (mydata):
            tabela.append(mydata)
    return tabela

def avg( a ):
    av=0.0
    for i in range (0,size(a)):
        av=av+a[i]
    return av/size(a)

def grafico( a ):
    y=range(0,size(a))
    plot (y,a,"ro")
    xlabel('SAMPLE')
    ylabel('DATA')
    show()

def histograma( a , titulo, eixox, eixoy ):
    h = []
    for x in a:
        h.append(float (x))
    hist(a)
    title(titulo)
    xlabel(eixox)
    ylabel(eixoy)
    show()
      
def readfile(name, column):
    f = open(name, 'r')
    lines=f.readlines()
    result=[]
    for x in lines:
        if (x.strip()):
            elem=(x.split(' ')[column]).strip()
            number=float(elem)
            result.append(number)
    f.close()
    return result

def writefile(name,array):
    f = open(name, 'w')
    for item in array:
        f.write(str(item)+"\n")
    f.close()

def column(matrix, i):
    return [row[i] for row in matrix]

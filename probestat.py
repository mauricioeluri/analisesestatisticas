#!/usr/bin/env python
# -*- coding: utf-8 -*-

from a2 import *

def media(v):
    soma = 0
    for val in v:
        soma += float(val)
    return soma / len(v)

def mediana(v):
    v.sort()
    if (len(v) % 2) == 0:
        arr = []
        arr.append(v[int(len(v) / 2)])
        arr.append(v[int(len(v) / 2) - 1])
        return media(arr)
    else:
        return v[int(len(v) / 2)]


def moda(v):
    b, modas = 2, []
    for i in v:
        a = v.count(i)
        if a > b:
            b, modas = a, [i]
        elif a == b and i not in modas:
            modas.append(i)
    modas.sort()
    if len(modas) == 0:
        return "NA"
    return modas

def desvio_padrao(v):
    return pow(variancia(v), .5)

def variancia(v):
    _media = media(v)
    soma = 0
    for valor in v:
        soma += (float(valor) - float(_media))**2
    return soma / (len(v) - 1)


def amplitude(v):
    v.sort()
    return float(v[int(len(v)) - 1]) - float(v[0])

def coef_variancia(v):
    return desvio_padrao(v) / media(v)

def main():
    
    user_input = raw_input("Selecione quais dados analizar:\n[1]Garotos\n[2]Garotas\n")
    
    opcao = ""
    
    if user_input == "1":
        opcao = "garotos"
    else:
        opcao = "garotas"
    
    z = readtable("%s.dat" % (opcao))
    dados = column(z, 0)    

    print "Média = %s" % (media(dados))
    print "Mediana = %s" % (mediana(dados))
    print "Moda(s) = %s" % (moda(dados))
    print "Desvio padrão = %s" % (desvio_padrao(dados))
    print "Variância = %s" % (variancia(dados))
    print "Amplitude = %s" % (amplitude(dados))
    print "Coeficiente de variação = %s" % (coef_variancia(dados))

main()
